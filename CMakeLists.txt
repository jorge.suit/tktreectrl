cmake_minimum_required( VERSION 3.0 )
project(treectrl)

# The version number.
set (TREECTRL_VERSION_MAJOR 2)
set(TREECTRL_VERSION_MINOR 4)
set(TREECTRL_VERSION_PATCH 2)
set(PACKAGE_VERSION "${TREECTRL_VERSION_MAJOR}.${TREECTRL_VERSION_MINOR}")
set(PACKAGE_PATCHLEVEL "${TREECTRL_VERSION_MAJOR}.${TREECTRL_VERSION_MINOR}.${TREECTRL_VERSION_PATCH}")

include(CheckIncludeFiles)
check_include_files("sys/param.h" HAVE_SYS_PARAM_H)
check_include_files("stdint.h" HAVE_STDINT_H)
check_include_files("stddef.h" STDC_HEADERS)
check_include_files("unistd.h" HAVE_UNISTD_H)

set(PACKAGE_NAME treectrl)

find_package(TclStub REQUIRED)

execute_process(COMMAND ${TCL_TCLSH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake/find_tcllib.tcl
  RESULT_VARIABLE FIND_LIB_STATUS
  OUTPUT_VARIABLE TCL_LIBPATH)

message(STATUS "FIND_LIB_STATUS = ${FIND_LIB_STATUS}")
message(STATUS "TCL_LIBPATH = ${TCL_LIBPATH}")

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  message(STATUS "setting CMAKE_INSTALL_PREFIX to ${TCL_LIBPATH}")
  set(CMAKE_INSTALL_PREFIX ${TCL_LIBPATH} CACHE PATH "" FORCE)
endif()

set(DEST_DIR "${CMAKE_PROJECT_NAME}${PACKAGE_VERSION}")
add_subdirectory(generic)
add_subdirectory(library)
